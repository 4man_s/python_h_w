list = ' + , - , / , // , * , % , ** '

first = input('Insert first number')
second = input('Insert second number')
operator = input('Insert math operator from ({})'.format(list))

if not first.isdigit() and not second.isdigit():
    exit('Please, insert numbers for first two arguments!')

if operator == '*':
    print(int(first) * int(second))
elif operator == '/':
    print(int(first) / int(second))
elif operator == '+':
    print(int(first) + int(second))
elif operator == '-':
    print(int(first) - int(second))
elif operator == '//':
    print(int(first) // int(second))
elif operator == '%':
    print(int(first) % int(second))
elif operator == '**':
    print(int(first) ** int(second))
else:
    exit('your math operator not in  ({})'.format(list))
