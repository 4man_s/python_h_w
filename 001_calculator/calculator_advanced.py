list = ' + , - , / , // , * , % , ** '
first = [None]


def execute(first, second, operator):
    if operator == '*':
        first[0] = int(first[0]) * int(second)
    elif operator == '/':
        first[0] = int(first[0]) / int(second)
    elif operator == '+':
        first[0] = int(first[0]) + int(second)
    elif operator == '-':
        first[0] = int(first[0]) - int(second)
    elif operator == '//':
        first[0] = int(first[0]) // int(second)
    elif operator == '%':
        first[0] = int(first[0]) % int(second)
    elif operator == '**':
        first[0] = int(first[0]) ** int(second)
    else:
        print('your math operator not in  ({})'.format(list))
        return False
    return True


while True:
    if first[0] is None:
        first[0] = input('Insert first number: ')

    second = input('Insert second number: ')

    if second == "" or second == "exit":
        break
    elif second == 'show':
        print(first[0])
        continue

    operator = input('Insert math operator from ({}): '.format(list))
    if operator == "" or operator == "exit":
        break
    elif operator == 'show':
        print(first[0])
        continue

    if not str(first[0]).isdigit() and not second.isdigit():
        print('Please, insert numbers for first two arguments!')
        continue

    result = execute(first, second, operator)
    if result:
        print(first[0])
    else:
        print('your math operator not in  ({})'.format(list))
