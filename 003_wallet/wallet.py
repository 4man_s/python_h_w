config = {}
config['filename'] = "wallet.csv"
config['params'] = [
    ["date", "Please enter the date of transaction in 'yyyy.mm.dd' format: "],
    ["text", "Please enter the the purpose of transaction: ", "Please "],
    ["digit", "Please enter the sum of transaction : ", "You entered wrong value, please use only digits"]]
config['exit_empty_msg'] = "Nothing was saved to wallet!"
config['hello_msg'] = "Hello, there!! You can access the list of commands using command 'help'"
config['help_msg'] = "List of existing commands:\n\t" \
                     "[add] - adds new record\n\t" \
                     "[total] - calculate total for all time\n\t" \
                     "[period_total] - calculate total for period\n\t" \
                     "[save] - saved transactions in wallet immediately\n\t" \
                     "[exit] - ends the program and all entered data will be saved in wallet(except not fully filled)\n\t" \
                     "[quit] - ends the program and all entered data will be saved in wallet(except not fully filled)"
config['fail_add'] = "Record wasn't saved, not all parameters entered, please try again"
first = True
buffer = []


import csv


def write_from_buffer(buffer):
    with open('wallet.csv', "w+") as w:
        for record in buffer:
            ...


def validate_input(param_input, inp_type):
    """
    function validates user input in 'add' action
    :param param_input: 
    :param inp_type: 
    :return: 
    """
    if inp_type == 'text':
        return True if len(param_input) > 0 else False
    elif inp_type == 'digit':
        return True if str(param_input).isdigit() else False
    elif inp_type == 'date':
        return True if str(param_input).isdigit() else False
    return


def define_action(u_input, buffer, config):
    """ 
    function defines action based on user input
    :param u_input: string
    :param buffer: list
    :return: string
    """
    if u_input == 'help':
        u_input = help_action()
        define_action(u_input, buffer, config)
    elif u_input == 'exit' or u_input == 'quit':
        if not buffer:
            print("Nothing was saved to wallet")
        else:
            write_from_buffer(buffer)
        exit()
    elif u_input == "total":
        return total_action(buffer)
    elif u_input == "period_total":
        return period_total_action(buffer)
    elif u_input == "add":
        return add_action(buffer, config)
    else:
        return config['help_msg']


def add_action(buffer, config):
    record = []
    for item in config['params']:
        valid = False
        while not valid:
            param_input = input(item[1])
            if validate_input(param_input, item[0]):
                record.append(param_input)
                valid = True
    if len(record) == 3:
        buffer.append(record)
        return record
    else:
        return config['fail_add']


def total_action(buffer):
    total = 0
    ...
    return int(total)


def period_total_action(buffer):
    total = 0
    ...
    return int(total)


def help_action(msg=config['help_msg']):
    u_inp = input(msg)
    return u_inp


while True:
    if first:
        print(config['hello_msg'])
        first = None
    u_input = input("Please enter the command: ")
    print(define_action(u_input, buffer, config))
    print(buffer)
