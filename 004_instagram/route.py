import insta_lib as lib
from flask import (
    Flask,
    render_template,
    request,
    redirect,
    send_from_directory,
    url_for
)

app = Flask(__name__)


@app.route('/add', methods=['POST', 'GET'])
def add():
    if request.form:
        print(request.form)

    styles = [{'link': '/static/css/main.css'}]
    return render_template('add_post.html', title='Add post/Instagram', styles=styles)


@app.route('/')
def index():
    styles = [{'link': '/static/css/main.css'}]
    # posts = lib.get_posts()
    posts = [{'title': '1', 'img': 'thrgfgxh',
              'desc': 'ergdfx ghrtttttt tx ghrtttttt tx ghrtttttt tx ghrtttttt ttttttt'},
             {'title': '2', 'img': 'dfbhfrch', 'desc': 'rrrrr rrrrrr rrrrrrrrrrr rrrrrrrr rrrrrr rrr rrrrrrrrr'},
             {'title': '3', 'img': 'rtfbhfrh', 'desc': 'eeeee eeee eee eee eee eee eee eee eee eee eee eeee'}]
    return render_template('main.html', title='Main/Instagram', styles=styles, posts=posts)


app.run(debug=True)
